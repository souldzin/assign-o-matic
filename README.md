# Assign-O-Matic

This is the repository for [the Assign-O-Matic](https://souldzin.gitlab.io/assign-o-matic/), a client-side web app that helps you randomly and secretly assign a group of people to eachother (like in a Secret-Gift-Exchange).

## Instructions

1. Install packages with:

   ```shell
   yarn
   ```

1. Start a development server with:

   ```shell
   yarn run build:dev
   ```
