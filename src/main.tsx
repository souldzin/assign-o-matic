import "./styles/main.pcss";
import React from "react";
import ReactDOM from "react-dom";
import { PageContext, PageContextType } from "./components/PageContext";

export const main = (Component, context: PageContextType) => {
  const container = document.getElementById("main");
  ReactDOM.render(
    <PageContext.Provider value={context}>
      <Component />
    </PageContext.Provider>,
    container
  );
};
