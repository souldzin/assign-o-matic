import React, { useState } from "react";
import { main } from "./main";
import { PageLayout } from "./components/PageLayout";
import { EncryptForm } from "./components/EncryptForm";

const Page = () => {
  const [value, setValue] = useState("");

  return (
    <PageLayout>
      <EncryptForm value={value} onChange={setValue} />
    </PageLayout>
  );
};

main(Page, { currentPage: "encrypt" });
