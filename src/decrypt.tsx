import React from "react";
import { main } from "./main";
import { PageLayout, SecretCodeForm } from "./components";

const Page = () => {
  return (
    <PageLayout>
      <SecretCodeForm render={(value) => <pre>{value}</pre>} />
    </PageLayout>
  );
};

main(Page, { currentPage: "decrypt" });
