import React, { useState } from "react";
import { REPO_URL, REPO_ISSUE_URL } from "./constants";
import { main } from "./main";
import { PageLayout, AssignmentsForm, AssignmentsResult } from "./components";
import { AssignmentsFormPayload, generateRandomAssignments } from "./lib";

const Page = () => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [result, setResult] = useState(null);

  const onSubmit = (payload: AssignmentsFormPayload) => {
    if (isSubmitting) {
      return;
    }

    setIsSubmitting(true);

    try {
      const report = generateRandomAssignments(payload);

      setResult({ report });
    } catch (e) {
      console.error(e);
      setResult({ error: e });
    }

    setIsSubmitting(false);
  };

  const goBack = () => {
    setResult(null);
  };

  const showForm = result === null;

  return (
    <PageLayout>
      <div className={showForm ? "" : "hidden"}>
        <div>
          <h2 className="mt-3 text-xl font-bold">
            Well, this is certainly a strange piece of internet...
          </h2>
          <p className="mt-3">
            This website will help you generate random secret assignments for
            events (e.g., a Secret-Gift-Exchange).
          </p>
          <p className="mt-3">
            Don't believe me? Try it out for yourself using the form below :)
          </p>
          <p className="mt-3">
            Have an issue or idea how to make this better? Then, by all means,
            please check out the <a href={REPO_URL}>GitLab project</a> and{" "}
            <a href={REPO_ISSUE_URL}>create an issue</a>.
          </p>
        </div>
        <div className="mt-8">
          <h2 className="text-xl font-bold">Let's get started!</h2>
          <AssignmentsForm className="mt-8" onSubmit={onSubmit} />
        </div>
      </div>
      {result ? <AssignmentsResult onSubmit={goBack} {...result} /> : null}
    </PageLayout>
  );
};

main(Page, { currentPage: "home" });
