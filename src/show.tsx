import React from "react";
import { main } from "./main";
import { PageLayout, SecretCodeForm, PayloadDisplay } from "./components";

const Page = () => {
  return (
    <PageLayout>
      <SecretCodeForm render={(value) => <PayloadDisplay value={value} />} />
    </PageLayout>
  );
};

main(Page, { currentPage: "show" });
