import { AssignmentPayload, AssignmentsFormPayload } from "./types";

export const serializePayload = (payload: AssignmentPayload) => {
  return JSON.stringify(payload);
};

export const tryParseToPayload = (message): AssignmentPayload => {
  try {
    const value = JSON.parse(message) as AssignmentPayload;

    if (!value.name) {
      console.error("Found JSON, but could not find `name`");
      return null;
    }

    // Only return valid fields
    value.fields = (value.fields || []).filter((x) => x.title && x.value);

    return value;
  } catch (e) {
    return null;
  }
};

export const verifyForm = (payload: AssignmentsFormPayload) => {
  const errors = [];

  const names = new Set<string>();

  if (payload.participants.length < 2) {
    errors.push(
      `Not enough recepients. Found ${payload.participants.length} but expected at least 2.`
    );
    return errors;
  }

  payload.participants.forEach((p) => {
    const cleanName = p.name.trim();

    if (!cleanName) {
      errors.push("Found participant with empty name.");
    } else if (names.has(cleanName)) {
      errors.push(
        `Found duplicate participant with name "${p.name}". Please rename or remove one of the participants.`
      );
    } else {
      names.add(cleanName);
    }
  });

  // Now that we have all names, let's verify the restrictions.
  payload.participants.forEach((p) => {
    p.restrict.forEach((name) => {
      const cleanName = name.trim();

      if (!names.has(cleanName)) {
        errors.push(
          `Found participant "${p.name}" who cannot be assigned with "${name}", but "${name}" is not a participant. Please remove this restriction or add a participant named "${name}".`
        );
      }
    });
  });

  return errors;
};
