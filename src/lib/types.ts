export type ParticipantField = {
  title: string;
  value: string;
};

export type Participant = {
  name: string;
  fields: ParticipantField[];
  restrict: string[];
};

export type AssignmentsFormPayload = {
  eventName: string;
  fields: string[];
  participants: ParticipantPayload[];
};

export type ParticipantPayload = {
  name: string;
  fieldValues: string[];
  restrict: string[];
};

export type AssignmentPayload = {
  eventName?: string;
  name: string;
  fields: ParticipantField[];
};

export type AssignmentsReportItem = {
  name: string;
  fields: ParticipantField[];
  link: string;
};

export type AssignmentsReport = {
  assignments: AssignmentsReportItem[];
};
