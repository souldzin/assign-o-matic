import CryptoJS from "crypto-js";

export function encrypt(message) {
  return CryptoJS.AES.encrypt(message, "secret").toString();
}

export function decrypt(message) {
  const bytes = CryptoJS.AES.decrypt(message, "secret");
  const originalText = bytes.toString(CryptoJS.enc.Utf8);

  return originalText;
}

export function decryptOrDefault(message, defaultValue = "") {
  try {
    return decrypt(message);
  } catch {
    return defaultValue;
  }
}
