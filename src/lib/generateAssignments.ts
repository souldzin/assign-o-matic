import { serializePayload } from "./payload";
import { encrypt } from "./crypto";
import {
  AssignmentPayload,
  AssignmentsFormPayload,
  AssignmentsReport,
  ParticipantPayload,
} from "./types";

const ERROR_RAN_OUT = "Ran out of eligible participants.";

export const isRanOutError = (e) => e.message === ERROR_RAN_OUT;

const throwRanOutError = () => {
  throw new Error(ERROR_RAN_OUT);
};

const getShowURL = (): string => {
  return new URL("./show.html", location.href).href;
};

const createAssignmentPayload = (
  participant: ParticipantPayload,
  formPayload: AssignmentsFormPayload
): AssignmentPayload => {
  return {
    eventName: formPayload.eventName,
    name: participant.name,
    fields: formPayload.fields.map((title, idx) => ({
      title,
      value: participant.fieldValues[idx] || "",
    })),
  };
};

const createAssignmentLink = (
  participant: ParticipantPayload,
  formPayload: AssignmentsFormPayload
): string => {
  const payload = createAssignmentPayload(participant, formPayload);
  const code = encrypt(serializePayload(payload));

  return `${getShowURL()}#${encodeURIComponent(code)}`;
};

const getRandomName = (names: Set<string>) => {
  const randIndex = Math.floor(Math.random() * names.size);

  return Array.from(names)[randIndex];
};

const getParticipantsByName = (payload: AssignmentsFormPayload) => {
  const map = new Map<string, ParticipantPayload>();

  payload.participants.forEach((p) => {
    map.set(p.name, p);
  });

  return map;
};

const getEligibleNames = (
  names: Set<string>,
  participant: ParticipantPayload
) => {
  const eligibleNames = new Set(names);
  eligibleNames.delete(participant.name);
  participant.restrict.forEach((x) => eligibleNames.delete(x));
  return eligibleNames;
};

const generateRandomAssignmentMap = (
  participants: Map<string, ParticipantPayload>
): Map<string, string> => {
  const nameAssignments = new Map<string, string>();
  const namesRemaining = new Set(participants.keys());

  // Sort by most restricted first.
  Array.from(participants.values())
    .sort((a, b) => b.restrict.length - a.restrict.length)
    .forEach((participant) => {
      const eligibleNames = getEligibleNames(namesRemaining, participant);

      if (!eligibleNames.size) {
        throwRanOutError();
      } else {
        const selection = getRandomName(eligibleNames);
        nameAssignments.set(participant.name, selection);
        namesRemaining.delete(selection);
      }
    });

  if (namesRemaining.size > 0) {
    throw new Error("Expected to have no remaning names, but found some.");
  }

  return nameAssignments;
};

/**
 * Because we can add restrictions, we need to retry our random algorithm because we
 * can end up in a hole.
 *
 * @param participants
 * @param retries
 */
const generateRandomAssignmentMapWithRetries = (
  participants: Map<string, ParticipantPayload>,
  retries: number = 10
): Map<string, string> => {
  try {
    return generateRandomAssignmentMap(participants);
  } catch (e) {
    if (retries > 0 && isRanOutError(e)) {
      return generateRandomAssignmentMapWithRetries(participants, retries - 1);
    } else {
      throw e;
    }
  }
};

const createReport = (
  nameAssignments: Map<string, string>,
  payload: AssignmentsFormPayload
): AssignmentsReport => {
  return {
    assignments: payload.participants.map((p) => {
      const assignedName = nameAssignments.get(p.name);
      const assignedParticipant = payload.participants.find(
        (x) => x.name === assignedName
      );

      const link = createAssignmentLink(assignedParticipant, payload);

      return {
        name: p.name,
        fields: payload.fields.map((title, i) => ({
          title,
          value: p.fieldValues[i] || "",
        })),
        link,
      };
    }),
  };
};

export const generateRandomAssignments = (
  payload: AssignmentsFormPayload
): AssignmentsReport => {
  const participantsByName = getParticipantsByName(payload);

  const nameAssignments = generateRandomAssignmentMapWithRetries(
    participantsByName
  );

  return createReport(nameAssignments, payload);
};
