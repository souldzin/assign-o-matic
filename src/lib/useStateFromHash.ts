import { useState, useEffect } from "react";

const getValueFromHash = () => {
  return decodeURIComponent(window.location.hash?.slice(1) || "");
};

export const useStateFromHash = () => {
  const [state, setState] = useState(getValueFromHash());

  const onHashChange = () => {
    setState(getValueFromHash());
  };

  const setStateAndHash = (value) => {
    setState(value);
    window.location.hash = `#${encodeURIComponent(value)}`;
  };

  useEffect(() => {
    window.addEventListener("hashchange", onHashChange, false);

    return () => {
      window.removeEventListener("hashchange", onHashChange, false);
    };
  }, []);

  return [state, setStateAndHash];
};
