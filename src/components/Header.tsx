import React from "react";
import { HeaderTitle } from "./HeaderTitle";
import { HeaderNav } from "./HeaderNav";

export const Header = () => {
  return (
    <header>
      <HeaderTitle />
      <HeaderNav />
    </header>
  );
};
