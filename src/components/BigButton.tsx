import React from "react";

export const BigButton = ({ className = "", disabled = false, ...attrs }) => {
  const colorClass = disabled
    ? "bg-gray-300 hover:bg-gray-100 focus:bg-gray-100"
    : "bg-green-500 hover:bg-green-300 focus:bg-green-300 text-white";
  return (
    <button
      className={`border rounded-full p-8 ${colorClass} ${className}`}
      disabled={disabled}
      {...attrs}
    >
      {attrs.children}
    </button>
  );
};
