import React from "react";
import { TextInput } from "./TextInput";
import { decryptOrDefault } from "../lib/crypto";
import { useStateFromHash } from "../lib/useStateFromHash";

type Props = {
  render: (string) => any;
};

export const SecretCodeForm = ({ render }: Props) => {
  const [code, setCode] = useStateFromHash();

  const decryptedValue = decryptOrDefault(code);

  return (
    <div>
      <h2 className="text-xl font-bold">
        <label htmlFor="secretCode">Please enter your secret code:</label>
      </h2>
      <TextInput
        id="secretCode"
        className="mt-3"
        value={code}
        onChange={(e) => setCode(e.target.value)}
      />
      <div className="mt-8">{render(decryptedValue)}</div>
    </div>
  );
};
