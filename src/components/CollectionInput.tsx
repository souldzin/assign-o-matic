import React from "react";

type Props<T> = {
  value: T[];
  onChange: (items: T[]) => void;
  isEmpty?: (value: T) => boolean;
  render: (args: {
    item: T;
    isEmpty: boolean;
    onChange: (T) => void;
    onBlur: (T) => void;
    index: number;
  }) => any;
};

export const CollectionInput = <T,>({
  value,
  onChange,
  isEmpty = (x) => !Boolean(x),
  render,
}: Props<T>) => {
  const updateItem = (item, idx) => {
    if (idx > value.length) {
      onChange([...value, item]);
    } else {
      onChange([...value.slice(0, idx), item, ...value.slice(idx + 1)]);
    }
  };

  const cleanItems = (val: T) => {
    if (isEmpty(val)) {
      onChange(value.filter((x) => !isEmpty(x)));
    }
  };

  return (
    <>
      {[...value, null].map((item, idx) => {
        const isEmpty = idx === value.length;

        return (
          <React.Fragment key={idx}>
            {render({
              item,
              isEmpty,
              onChange: (val) => updateItem(val, idx),
              onBlur: (val) => cleanItems(val),
              index: idx,
            })}
          </React.Fragment>
        );
      })}
    </>
  );
};
