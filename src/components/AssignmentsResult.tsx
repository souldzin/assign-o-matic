import React from "react";
import { AssignmentsReport, isRanOutError } from "../lib";
import { BigButton } from "./BigButton";

type BodyProps = {
  report?: AssignmentsReport;
  error?: Error;
};

type Props = {
  report?: AssignmentsReport;
  error?: Error;
  onSubmit: () => void;
};

const AssignmentResultLayout = ({ onSubmit, ...attrs }) => {
  return (
    <div>
      {attrs.children}
      <div className="flex justify-center my-6">
        <BigButton onClick={onSubmit}>Go back</BigButton>
      </div>
    </div>
  );
};

const AssignmentResultContent = (props: BodyProps) => {
  if (props.error) {
    return (
      <div>
        <h2 className="mt-3 text-xl font-bold">Uh oh!</h2>
        {isRanOutError(props.error) ? (
          <p>
            We were not able to generate random assignments with the given
            participants and constraints. This can happen sometimes if
            restrictions are set up that are impossible to resolve. Also,
            unfortunately, our algorithm is not perfect... Please go back and
            review your input, then try again and hope for the best!
          </p>
        ) : (
          <p>
            Uh oh! An unexpected error happened while generating the report.
            This shouldn't happen. Please consider going back and trying again.
          </p>
        )}
      </div>
    );
  }

  return (
    <div>
      <h2 className="mt-3 text-xl font-bold">Tada!</h2>
      <p>
        Here are the participants with their assignment links. Secretly send
        these links out to your participants.
      </p>
      <table className="mt-8 table-fixed">
        <thead>
          <tr>
            <th className="w-1/3">Name</th>
            <th className="w-2/3">Assignment Link</th>
          </tr>
        </thead>
        <tbody>
          {props.report.assignments.map(({ name, fields, link }) => {
            return (
              <tr key={name}>
                <td>{name}</td>
                <td className="text-sm">
                  <a href={link}>{link}</a>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export const AssignmentsResult = ({ onSubmit, ...bodyProps }: Props) => {
  return (
    <AssignmentResultLayout onSubmit={onSubmit}>
      <AssignmentResultContent {...bodyProps} />
    </AssignmentResultLayout>
  );
};
