import React from "react";

const CHARACTERS = "🅰🆂🆂🅸🅶🅽-🅾-🅼🅰🆃🅸🅲".split("");

const COLOR_CLASSES = [
  "text-red-500",
  "text-red-600",
  "text-red-700",
  "text-red-800",
  "text-yellow-500",
  "text-yellow-600",
  "text-yellow-700",
  "text-yellow-800",
  "text-green-500",
  "text-green-600",
  "text-green-700",
  "text-green-800",
  "text-blue-500",
  "text-blue-600",
  "text-blue-700",
  "text-blue-800",
  "text-indigo-500",
  "text-indigo-600",
  "text-indigo-700",
  "text-indigo-800",
  "text-purple-500",
  "text-purple-600",
  "text-purple-700",
  "text-purple-800",
  "text-pink-500",
  "text-pink-600",
  "text-pink-700",
  "text-pink-800",
];

const getColorClass = () => {
  const randIndx = Math.floor(Math.random() * COLOR_CLASSES.length);

  return COLOR_CLASSES[randIndx];
};

export const HeaderTitle = React.memo(() => {
  const letters = CHARACTERS.map((char, index) => (
    <span className={getColorClass()} key={`${char}_${index}`}>
      {char}
    </span>
  ));
  return <h1 className="text-4xl font-bold text-center">{letters}</h1>;
});
