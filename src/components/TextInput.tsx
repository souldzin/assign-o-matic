import React from "react";

export const TextInput = ({ className = "", value, ...attrs }) => {
  return (
    <input
      type="text"
      value={value}
      className={`form-input ${className}`}
      {...attrs}
    />
  );
};
