import React, { useContext } from "react";
import { NAV } from "../constants";
import { PageContext } from "./PageContext";

export const HeaderNav = () => {
  const { currentPage } = useContext(PageContext);

  const items = NAV.map(({ text, key, href }) => {
    const className = currentPage === key ? "font-bold" : "";

    return (
      <li key={key}>
        <a className={className} href={href}>
          {text}
        </a>
      </li>
    );
  });

  return (
    <nav className="header-nav my-3">
      <ul className="flex justify-center">{items}</ul>
    </nav>
  );
};
