import React from "react";
import { TextAreaInput } from "./TextAreaInput";
import { encrypt } from "../lib/crypto";

export const EncryptForm: React.FC<{
  value: string;
  onChange: (string) => void;
}> = ({ value, onChange }) => {
  const encryptedValue = encrypt(value);

  return (
    <div>
      <TextAreaInput
        className="mt-3"
        value={value}
        onChange={(e) => onChange(e.target.value)}
      />
      <div className="mt-3">{encryptedValue}</div>
    </div>
  );
};
