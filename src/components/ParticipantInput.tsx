import React from "react";
import { ParticipantPayload } from "../lib";
import { CollectionInput } from "./CollectionInput";
import { TextInput } from "./TextInput";
import { TextAreaInput } from "./TextAreaInput";

type Props = {
  value: ParticipantPayload;
  onChange: (x: ParticipantPayload) => void;
  onBlur: (x: ParticipantPayload) => void;
  fields: string[];
  className?: string;
  participantId?: string;
};

export const ParticipantInput = ({
  value,
  onChange,
  onBlur,
  fields,
  className,
  participantId,
}: Props) => {
  const setName = (name: string) => {
    onChange({
      ...value,
      name,
    });
  };

  const setFieldValue = (idx, fieldValue: string) => {
    onChange({
      ...value,
      // Make sure we have a 1:1 mapping from fieldValues to fields
      fieldValues: fields.map((_, fieldIdx) => {
        if (fieldIdx === idx) {
          return fieldValue;
        }

        return value.fieldValues[fieldIdx] || "";
      }),
    });
  };

  const setRestrictions = (restrict: string[]) => {
    onChange({
      ...value,
      restrict,
    });
  };

  const fieldInputs = fields.map((title, idx) => {
    const fieldValue = value.fieldValues[idx] || "";

    return (
      <React.Fragment key={`${idx}_${title}`}>
        <p className="mt-5">{title}</p>
        <TextAreaInput
          className="mt-1"
          value={fieldValue}
          onChange={(e) => setFieldValue(idx, e.target.value)}
        />
      </React.Fragment>
    );
  });

  return (
    <div className={`relative ${className}`} onBlur={() => onBlur(value)}>
      {participantId ? (
        <span className="text-xs absolute top-0.5 left-0.5">
          {participantId}
        </span>
      ) : null}
      <p>Name:</p>
      <TextInput
        className="mt-1"
        value={value.name}
        onChange={(e) => setName(e.target.value)}
        placeholder="Example: Jane Doe"
      />
      {fieldInputs}
      <p className="mt-5">Do not assign with (optional):</p>
      <CollectionInput<string>
        value={value.restrict}
        onChange={setRestrictions}
        render={({ item, isEmpty, onChange, onBlur }) => (
          <TextInput
            className="mt-1 mb-2"
            placeholder="Name of other participant..."
            value={isEmpty ? "" : item}
            onChange={(e) => onChange(e.target.value)}
            onBlur={(e) => onBlur(e.target.value)}
          />
        )}
      />
    </div>
  );
};
