import React, { useState } from "react";
import { AssignmentPayload } from "../lib";
import { BigButton } from "./BigButton";

type Props = {
  value: AssignmentPayload;
};

const AssignmentPayloadDisplayField: React.FC = (props: {
  title: string;
  value: string;
  className: string;
}) => {
  return (
    <div className={`border rounded px-6 py-4 ${props.className}`}>
      <h3>{props.title}</h3>
      <pre className="text-sm mt-3">{props.value}</pre>
    </div>
  );
};

const AssignmentPayloadHide = ({ onClick }) => {
  return (
    <div className="flex justify-center items-center">
      <BigButton onClick={onClick}>Click to reveal your assignment!</BigButton>
    </div>
  );
};

export const AssignmentPayloadDisplay = ({ value }: Props) => {
  const [isHidden, setHidden] = useState(true);

  if (isHidden) {
    return <AssignmentPayloadHide onClick={() => setHidden(false)} />;
  }

  const fields = value.fields.map((field) => (
    <AssignmentPayloadDisplayField
      key={field.title}
      className="mt-3"
      {...field}
    />
  ));

  const headerMessage = value.eventName ? (
    <span>
      Your secret assignment for the <strong>{value.eventName}</strong> is...
      <strong>{value.name}</strong>
    </span>
  ) : (
    <span>
      Your secret assignment is...
      <strong>{value.name}</strong>
    </span>
  );

  return (
    <div>
      <h2 className="text-xl">{headerMessage}</h2>
      {fields}
    </div>
  );
};
