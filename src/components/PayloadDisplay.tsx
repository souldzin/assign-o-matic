import React from "react";
import { tryParseToPayload } from "../lib";
import { AssignmentPayloadDisplay } from "./AssignmentPayloadDisplay";

const PayloadDisplayError = () => (
  <p>Decoded the message, but could not parse the contents.</p>
);

export const PayloadDisplay = ({ value }) => {
  if (!value) {
    return null;
  }

  const payload = tryParseToPayload(value);

  if (!payload) {
    return <PayloadDisplayError />;
  }

  return <AssignmentPayloadDisplay value={payload} />;
};
