import React from "react";
import { NavKey } from "../constants";

export type PageContextType = {
  currentPage: NavKey;
};

export const PageContext = React.createContext<PageContextType>({
  currentPage: "home",
});
