import React from "react";

export const TextAreaInput = ({ className, value, ...attrs }) => {
  return (
    <textarea
      value={value}
      className={`form-input ${className}`}
      {...attrs}
    ></textarea>
  );
};
