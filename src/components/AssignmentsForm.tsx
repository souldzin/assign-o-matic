import React, { useState } from "react";
import { ParticipantPayload, AssignmentsFormPayload, verifyForm } from "../lib";
import { CollectionInput } from "./CollectionInput";
import { TextInput } from "./TextInput";
import { ParticipantInput } from "./ParticipantInput";
import { BigButton } from "./BigButton";

const EMPTY_PARTICIPANT: ParticipantPayload = {
  name: "",
  fieldValues: [],
  restrict: [],
};

const isEmptyParticipant = (x: ParticipantPayload) => {
  if (!x) {
    return true;
  }

  return !x.name && !x.fieldValues.some(Boolean);
};

const AssignmentFormErrors = ({ errors = [], ...attrs }) => {
  if (!errors.length) {
    return null;
  }

  return (
    <div {...attrs}>
      <p>
        Uh oh! Please resolve the following errors then try hitting the button
        again :)
      </p>
      <ul className="list-disc ml-6 mt-3 text-red-500">
        {errors.map((text, idx) => (
          <li key={idx}>{text}</li>
        ))}
      </ul>
    </div>
  );
};

type Props = {
  className: string;
  onSubmit?: (payload: AssignmentsFormPayload) => any;
};

export const AssignmentsForm = ({
  className = "",
  onSubmit = () => {},
}: Props) => {
  const [eventName, setEventName] = useState("");
  const [fieldNames, setFieldNames] = useState([]);
  const [participants, setParticipants] = useState([] as ParticipantPayload[]);
  const [errors, setErrors] = useState([]);

  const submit = () => {
    setErrors([]);

    const payload: AssignmentsFormPayload = {
      eventName,
      fields: fieldNames,
      participants,
    };

    const verificationErrors = verifyForm(payload);

    if (verificationErrors.length) {
      setErrors(verificationErrors);
    } else {
      onSubmit(payload);
    }
  };

  return (
    <div className={className}>
      <h3>
        <label htmlFor="eventName">
          Please enter the name of the event (optional):
        </label>
      </h3>
      <TextInput
        id="eventName"
        placeholder="Example: The Super Fun Secret Explosive Gift Exchange"
        className="mt-3"
        value={eventName}
        onChange={(e) => setEventName(e.target.value)}
      />
      <h3 className="mt-3">
        <label htmlFor="emptyFieldName">
          Please enter fields you'd like to include for each participant
          (optional):
        </label>
      </h3>
      <CollectionInput<string>
        value={fieldNames}
        onChange={setFieldNames}
        render={({ item: name, isEmpty, onChange, onBlur }) => (
          <TextInput
            id={isEmpty ? "emptyFieldName" : null}
            placeholder="Example: What's your favorite cereal?"
            className="mt-3"
            value={isEmpty ? "" : name}
            onChange={(e) => onChange(e.target.value)}
            onBlur={(e) => onBlur(e.target.value)}
          />
        )}
      />
      <h3 className="mt-3">
        Please enter the participants who will be randomly assigned to
        eachother:
      </h3>
      <div className="flex flex-wrap">
        <CollectionInput<ParticipantPayload>
          value={participants}
          onChange={setParticipants}
          isEmpty={isEmptyParticipant}
          render={({ item, isEmpty, onChange, onBlur, index }) => (
            <ParticipantInput
              className="mt-3 mx-3 max-w-md p-5 border rounded bg-gray-50"
              fields={fieldNames}
              value={isEmpty ? EMPTY_PARTICIPANT : item}
              onChange={(val) => onChange(val)}
              onBlur={(val) => onBlur(val)}
              participantId={isEmpty ? null : `${index + 1}`}
            />
          )}
        />
      </div>
      <div className="flex justify-center my-6">
        <BigButton onClick={submit} disabled={participants.length < 2}>
          Click to submit
        </BigButton>
      </div>
      <AssignmentFormErrors errors={errors} />
    </div>
  );
};
