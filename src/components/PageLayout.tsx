import React from "react";
import { Header } from "./Header";

export const PageLayout: React.FC = ({ children }) => {
  return (
    <div className="my-12 mx-auto max-w-6xl px-5">
      <Header />
      {children}
    </div>
  );
};
