type NavKey = "home" | "show" | "encrypt" | "decrypt";

type NavObj = {
  key: NavKey;
  text: string;
  href: string;
};

export { NavKey };

export const NAV: NavObj[] = [
  { key: "home", text: "Home", href: "./index.html" },
  { key: "show", text: "Reveal Code", href: "./show.html" },
];

export const REPO_URL = "https://gitlab.com/souldzin/assign-o-matic";
export const REPO_ISSUE_URL =
  "https://gitlab.com/souldzin/assign-o-matic/-/issues/new";
